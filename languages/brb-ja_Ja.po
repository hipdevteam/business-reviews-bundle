msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: ja_JA\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.4.2\n"

msgid "%s Rating"
msgstr "%s 評価"

msgid "See all reviews"
msgstr "すべてのクチコミを見る"

msgid "Google User"
msgstr "Google ユーザー"

msgid "read more"
msgstr "もっと読む"

msgid "More reviews"
msgstr "次のクチコミ"

msgid "Based on %s reviews"
msgstr "%sレビューの平均"

msgid "Based on %s reviews from"
msgstr "%sレビューの平均"

msgid "review us on"
msgstr "にクチコミを書く"

msgid "just left us a %s star review"
msgstr "ちょうど%sレビューを残しました"

msgid "on"
msgstr "の上"

msgid "Excellent"
msgstr "素晴らしい"

msgid "Great"
msgstr "より大きな"

msgid "Good"
msgstr "良い"

msgid "Fair"
msgstr "公平"

msgid "Poor"
msgstr "貧しい"
