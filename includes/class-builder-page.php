<?php

namespace WP_Business_Reviews_Bundle\Includes;

use WP_Business_Reviews_Bundle\Includes\Core\Core;
use WP_Business_Reviews_Bundle\Includes\View\View;

class Builder_Page {

    private $core;
    private $view;
    private $collection_deserializer;

    public function __construct(Collection_Deserializer $collection_deserializer, Core $core, View $view) {
        $this->collection_deserializer = $collection_deserializer;
        $this->core = $core;
        $this->view = $view;
    }

    public function register() {
        add_action('brb_admin_page_brb-builder', array($this, 'init'));
    }

    public function init() {
        if (isset($_GET['brb_notice'])) {
            $this->add_admin_notice();
        }

        $collection = null;
        if (isset($_GET['brb_collection_id'])) {
            $collection = $this->collection_deserializer->get_collection($_GET['brb_collection_id']);
        }

        $this->render($collection, isset($_GET['brb_clone']));
    }

    public function add_admin_notice($notice_code = 0) {
        //TODO
    }

    public function render($collection, $is_clone = false) {
        global $wp_version;
        if (version_compare($wp_version, '3.5', '>=')) {
            wp_enqueue_media();
        }

        $collection_id = '';
        $collection_post_title = '';
        $collection_content = '';
        $collection_inited = false;
        $businesses = null;
        $reviews = null;

        if ($collection != null) {
            if (!$is_clone) {
                $collection_id = $collection->ID;
                $collection_post_title = $collection->post_title;
            }
            $collection_content = trim($collection->post_content);

            $data = $this->core->get_reviews($collection);
            if ($data !== false) {
                $businesses = $data['businesses'];
                $reviews = $data['reviews'];
                $options = $data['options'];
                $errors = $data['errors'];
                if (isset($businesses) && count($businesses) || isset($reviews) && count($reviews)) {
                    $collection_inited = true;
                }
            }
        }

        $google_places_api = get_option('brb_google_places_api');

        $auth_code = get_option('brb_auth_code');
        $auth_code_test = get_option('brb_auth_code_test');
        $auth_code = isset($auth_code_test) && strlen($auth_code_test) > 0 ? $auth_code_test : $auth_code;

        $license = get_option('brb_license');

        $layout_old = get_option('brb_layout_old');

        ?>
        <div class="brb-builder">
            <form method="post" action="<?php echo esc_url(admin_url('admin-post.php?action=brb_collection_save')); ?>">
                <?php wp_nonce_field('brb_wpnonce', 'brb_nonce'); ?>
                <input type="hidden" id="brb_post_id" name="brb_collection[post_id]" value="<?php echo esc_attr($collection_id); ?>">
                <input type="hidden" id="brb_current_url" name="brb_collection[current_url]" value="<?php echo home_url($_SERVER['REQUEST_URI']); ?>">
                <?php if (isset($errors) && count($errors) > 0) { ?>
                <input type="hidden" id="brb_errors" value='<?php echo json_encode($errors); ?>'>
                <?php } ?>
                <div class="brb-builder-workspace">
                    <div class="brb-toolbar">
                        <div class="brb-toolbar-title">
                            <input class="brb-toolbar-title-input" type="text" id="brb_title" name="brb_collection[title]" value="<?php if (isset($collection_post_title)) { echo $collection_post_title; } ?>" placeholder="Enter a collection name" maxlength="255" autofocus>
                        </div>
                        <div class="brb-toolbar-control">
                            <?php if ($collection_inited) { ?>
                            <label><span id="brb_sc_msg">Shortcode </span><input id="brb_sc" type="text" value="[brb_collection id=<?php echo esc_attr($collection_id); ?>]" data-brb-shortcode="[brb_collection id=<?php echo esc_attr($collection_id); ?>]" onclick="this.select(); document.execCommand('copy'); window.brb_sc_msg.innerHTML = 'Shortcode Copied! ';" readonly/></label>
                            <div class="brb-toolbar-options">
                                <label title="Sometimes, you need to use this shortcode in PHP, for instance in header.php or footer.php files, in this case use this option"><input type="checkbox" onclick="var el = window.brb_sc; if (this.checked) { el.value = '&lt;?php echo do_shortcode( \'' + el.getAttribute('data-brb-shortcode') + '\' ); ?&gt;'; } else { el.value = el.getAttribute('data-brb-shortcode'); } el.select();document.execCommand('copy'); window.brb_sc_msg.innerHTML = 'Shortcode Copied! ';"/>Use in PHP</label>
                                <label title="You can use this code to show reviews on any site (not in WordPress), for instance on HTML Landing Page"><input type="checkbox" onclick="var el = window.brb_sc; if (this.checked) { el.value = '<div id=&#34;brb_collection_<?php echo esc_attr($collection_id); ?>&#34;></div><script type=&#34;text/javascript&#34;>!function(e){var c=document.createElement(&#34;script&#34;);c.src=e,document.body.appendChild(c)}(&#34;<?php echo admin_url('admin-ajax.php'); ?>?action=brb_embed&brb_collection_id=<?php echo esc_attr($collection_id); ?>&brb_callback=brb_&#34;+(new Date).getTime());</script>'; } else { el.value = el.getAttribute('data-brb-shortcode'); } el.select();document.execCommand('copy'); window.brb_sc_msg.innerHTML = 'Shortcode Copied! ';"/>Use as embedded code in HTML/JS</label>
                            </div>
                            <?php } ?>
                            <div class="brb-toolbar-devices">
                                <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24" data-dim="400"><title>400px</title><path d="M280-40q-33 0-56.5-23.5T200-120v-720q0-33 23.5-56.5T280-920h400q33 0 56.5 23.5T760-840v720q0 33-23.5 56.5T680-40H280Zm0-200v120h400v-120H280Zm200 100q17 0 28.5-11.5T520-180q0-17-11.5-28.5T480-220q-17 0-28.5 11.5T440-180q0 17 11.5 28.5T480-140ZM280-320h400v-400H280v400Zm0-480h400v-40H280v40Zm0 560v120-120Zm0-560v-40 40Z"/></svg>
                                <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24" data-dim="700"><title>700px</title><path d="M480-140q17 0 28.5-11.5T520-180q0-17-11.5-28.5T480-220q-17 0-28.5 11.5T440-180q0 17 11.5 28.5T480-140ZM200-40q-33 0-56.5-23.5T120-120v-720q0-33 23.5-56.5T200-920h560q33 0 56.5 23.5T840-840v720q0 33-23.5 56.5T760-40H200Zm0-200v120h560v-120H200Zm0-80h560v-400H200v400Zm0-480h560v-40H200v40Zm0 0v-40 40Zm0 560v120-120Z"/></svg>
                                <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24"><title>Full Screen</title><path d="M80-160q-33 0-56.5-23.5T0-240h160q-33 0-56.5-23.5T80-320v-440q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v440q0 33-23.5 56.5T800-240h160q0 33-23.5 56.5T880-160H80Zm400-40q17 0 28.5-11.5T520-240q0-17-11.5-28.5T480-280q-17 0-28.5 11.5T440-240q0 17 11.5 28.5T480-200ZM160-320h640v-440H160v440Zm0 0v-440 440Z"/></svg>
                            </div>
                            <button id="collsave" type="submit" class="button button-primary">Save & Refresh</button>
                        </div>
                    </div>
                    <div class="brb-builder-preview">
                        <span id="brb_builder_size"></span>
                        <textarea id="brb-builder-connection" name="brb_collection[content]" style="display:none"><?php echo $collection_content; ?></textarea>
                        <div id="brb_collection_preview">
                            <?php
                            if ($collection_inited) {
                                echo $this->view->render($collection_id, $businesses, $reviews, $options);
                            } else {
                                ?>To show reviews in this preview, firstly connect services on the right menu (Google, Facebook and etc.) and click '<b>Save & Refresh</b>' button. Then you can use this created collection as a widget or shortcode.<?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div id="brb-builder-option" class="brb-builder-option"></div>
            </form>
        </div>
        <script>
        jQuery(document).ready(function($) {
            function rplg_builder_init_listener(attempts) {
                if (!window.rplg_builder_init) {
                    if (attempts > 0) {
                        setTimeout(function() { rplg_builder_init_listener(attempts - 1); }, 200);
                    }
                    return;
                }
                rplg_builder_init($, {
                    el: '#brb-builder-option',
                    auth_code: '<?php echo $auth_code; ?>',
                    license: '<?php echo $license; ?>',
                    use_gpa: <?php echo $google_places_api === true || $google_places_api == 'true' ? 'true' : 'false'; ?>,
                    layout_old: <?php echo ((isset($options) && $options->layout_new && in_array($options->view_mode, BRB_NEW_LAYOUTS)) || $layout_old != '1')  ? '0' : '1'; ?>,
                    <?php if (strlen($collection_content) > 0) { echo 'conns: ' . $collection_content; } ?>
                });
            }
            rplg_builder_init_listener(20);
        });
        </script>
        <style>
            .update-nag { display: none; }
        </style>
        <?php
    }
}
