<?php

namespace WP_Business_Reviews_Bundle\Includes\Admin;

class Admin_Notice_Layout {

    public function register() {
        add_action('admin_notices', array($this, 'layout'));
    }

    public function layout() {

        $notice = isset($_GET['brb_notice_layout']) ? $_GET['brb_notice_layout'] : '';
        if ($notice == 'ok') {
            update_option('brb_notice_layout', 'ok');
            return;
        }

        $notice_layout = get_option('brb_notice_layout');

        if ($notice_layout != 'ok') {
            $class = 'notice notice-error is-dismissible';
            $url = remove_query_arg(array('taction', 'tid', 'sortby', 'sortdir', 'opt'));
            $url_later = esc_url(add_query_arg('brb_notice_layout', 'ok', $url));

            $notice = '<p style="font-weight:600;font-size:15px;">' .
                          'Please review your Rich Plugins layouts due to the latest update' .
                      '</p>' .
                      '<p style="font-weight:normal;font-size:15px;">' .
                          'Latest update has huge changes in List, Grid and Slider layouts. ' .
                          'The loading and performance speed has been increased many times, a powerful style editor has been added, ' .
                          'many errors in previous styles have been fixed and new opportunities appeared.' .
                      '</p>' .
                      '<p style="font-weight:normal;font-size:15px;">' .
                          'However, if you have the opportunity to return to the old layout, to do this, ' .
                          'click on the link below to go to <b>Settings</b> and select <b>Use old layouts: <u>enabled</u></b>.' .
                      '</p>' .
                      '<p>' .
                          '<a href="' . admin_url('admin.php?page=brb-settings&brb_tab=advance') . '" style="text-decoration:none;margin:0 10px 0 0;">' .
                              '<button class="button button-primary">Go to Settings</button>' .
                          '</a>' .
                          '<a href="' . $url_later . '" style="text-decoration:none;">' .
                              '<button class="button button-secondary">Ok, understand</button>' .
                          '</a>' .
                      '</p>';

            printf('<div class="%1$s" style="position:fixed;top:50px;right:20px;padding-right:30px;z-index:9999;margin-left:20px;max-width:600px">%2$s</div>', esc_attr($class), $notice);
        }
    }
}
