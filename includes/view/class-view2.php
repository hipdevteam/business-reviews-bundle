<?php

namespace WP_Business_Reviews_Bundle\Includes\View;

class View2 {

    private $view_helper;
    private $view_reviews;

    public function __construct(View_Reviews $view_reviews, View_Helper $view_helper) {
        $this->view_reviews = $view_reviews;
        $this->view_helper = $view_helper;
    }

    public function render($coll_id, $bizs, $revs, $opts) {
        $layout = preg_replace('/(_[a-z]*|\d)/', '', $opts->view_mode);
        $opts->view_mode = $layout;
        ?><div class="rpi<?php echo $this->get_classes($layout, $opts); ?>"
               style="<?php echo $opts->style_vars; ?>"
               data-id="<?php echo $coll_id; ?>"
               data-opts='<?php echo $this->options($opts); ?>'
               data-exec=""
        ><?php
        if (!isset($opts->ajax_load) || !$opts->ajax_load) {
            switch ($layout) {
                case 'tag':
                    $this->render_tag($coll_id, $bizs, $revs, $opts);
                    break;
                case 'flash':
                    $this->render_flash($coll_id, $bizs, $revs, $opts);
                    break;
                default:
                    $this->layout($coll_id, $bizs, $revs, $opts);
            }
        }
        ?><img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="js_loader" onload="(function(el) { var t = setInterval(function() {if (window.RichPlugins && !el.getAttribute('data-exec')) { let l = '<?php echo $layout; ?>'; RichPlugins.init(el); clearInterval(t) }}, 200)})(this.closest('.rpi'))" width="1" height="1" style="display:none">
        </div><?php
    }

    private function get_classes($layout, $opts) {
        switch ($layout) {
            case 'tag':
                $cls = ['', 'rpi-tag'];
                if ($opts->tag_popup > 0) {
                    array_push($cls, 'rpi-pop');
                }
                if (strlen($opts->tag_pos) > 0) {
                    array_push($cls, $opts->tag_pos);
                }
                if ($opts->tag_push) {
                    array_push($cls, 'push');
                }
                if ($opts->tag_expand) {
                    array_push($cls, 'expand');
                }
                return implode(' ', $cls);
        }
        return '';
    }

    public function render_tag($coll_id, $bizs, $revs, $opts) {

        $inner_cls = array();
        if ($opts->tag_review) {
            array_push($inner_cls, 'rpi-tag-review');
        }

        $stars_info = implode(',', [$bizs[0]->rating, '', '']);
        $stars_info2 = implode(',', [5, '', '']);

        if ($opts->tag_text) { ?><div class="rpi-tag-text"><?php echo $opts->tag_text; ?></div><?php } ?>
        <div class="rpi-tag-cnt" data-id="<?php echo $coll_id; ?>" data-opts='<?php echo $this->tag_options($opts); ?>' class="<?php echo implode(' ', $inner_cls); ?>" <?php $this->render_tag_click($bizs, $opts); ?>>
                <?php $this->logos($bizs[0]); ?>
                <div class="rpi-stars" data-info="<?php echo $stars_info; ?>">
                    <?php echo $this->stars($bizs[0]->rating); ?>
                </div>
                <?php if ($opts->tag_review) { ?>
                <div class="rpi-stars" data-info="<?php echo $stars_info2; ?>" data-reviewus="<?php echo $this->get_writereview_url($bizs[0]); ?>"></div>
                <?php } ?>
                <div class="rpi-tag-grade"><?php echo $bizs[0]->rating; ?></div>
        </div>
        <?php
    }

    public function render_tag_click($bizs, $opts) {
        switch ($opts->tag_click) {
            case 'reviews':
                $this->view_helper->window_open($this->get_allreview_url($bizs[0]), $opts);
                break;
            case 'link':
                $this->view_helper->window_open($opts->tag_link, $opts);
                break;
        }
    }

    public function render_flash($coll_id, $bizs, $revs, $opts) {
        ?>
        <div class="rpi-flash<?php if ($opts->flash_hide_mobile) { ?> rpi-flash-hide<?php } ?>" data-opts='<?php echo $this->flash_options($opts); ?>'>
            <div class="rpi-flash-wrap<?php if ($opts->flash_pos == 'right') { ?> rpi-flash-right<?php } ?>">
                <div class="rpi-flash-content">
                    <div class="rpi-flash-card">
                        <div class="rpi-flash-story rpi-flex" data-idx="1"></div>
                        <div class="rpi-flash-form rpi-content"><?php $this->reviews($revs, $opts); ?></div>
                    </div>
                    <div class="rpi-x"></div>
                </div>
            </div>
        </div>
        <?php
    }

    public function layout($coll_id, $bizs, $revs, $opts) {
        $layout_opts = '';
        $cnt_class = ' rpi-' . $opts->view_mode;
        switch ($opts->view_mode) {
            case 'slider':
                $layout_opts = $this->slider_options($opts);
            case 'grid':
                $cnt_class .= ' rpi-sh';
        }
        ?><div class="rpi-cnt rpi-col-m<?php echo $cnt_class; ?>"
               data-opts='<?php echo $layout_opts; ?>'
               style="visibility:var(--visible, hidden)"
        ><?php
            if (count($bizs) > 0) $this->header($bizs, $opts);
            if (count($revs) > 0) $this->content($revs, $opts);
        ?></div><?php
    }

    private function header($bizs, $opts) {
        $head_pos = $opts->view_mode == 'slider' ? $opts->slider_head_pos : $opts->head_pos;
        $head_class = ' rpi-header_' . ($head_pos == '1' ? 'top' : 'row') .
                      ($opts->view_mode == 'slider' && !$opts->slider_hide_nextprev ? ' rpi-header_pad' : '');
        ?><div class="rpi-header<?php echo $head_class; ?>">
            <div class="rpi-cards"><?php foreach ($bizs as $biz) $this->rating($biz, $opts); ?></div>
        </div><?php
    }

    private function content($revs, $opts) {
        $layout = $opts->view_mode;
        $head_pos = $layout == 'slider' ? $opts->slider_head_pos : $opts->head_pos;
        $content_class = ' rpi-content_' . ($head_pos == '1' ? 'top' : 'row');

        ?><div class="rpi-content<?php echo $content_class; ?>"><?php
            if ($layout == 'slider' && !$opts->slider_hide_nextprev) echo '<div class="rpi-slider-prev" tabindex="0"></div>';
            $this->reviews($revs, $opts);
            if ($layout == 'slider' && !$opts->slider_hide_nextprev) echo '<div class="rpi-slider-next" tabindex="0"></div>';
            if ($layout == 'slider' && !$opts->slider_hide_pagin) echo '<div class="rpi-dots-wrap"><div class="rpi-dots"></div></div>';
        ?></div><?php
    }

    private function rating($biz, $opts) {
        $head_pos = $opts->view_mode == 'slider' ? $opts->slider_head_pos : $opts->head_pos;
        if ($biz->provider == 'summary') {
        ?><div class="rpi-card rpi-card_block" data-provider="<?php echo implode(',', $biz->platform); ?>">
            <div class="rpi-card-inner rpi-flexwrap"><?php $this->logos($biz, true);
        } else {
        ?><div class="rpi-card<?php if ($head_pos == '2') { ?> rpi-card_block<?php } ?>" data-provider="<?php echo $biz->provider; ?>">
            <div class="rpi-card-inner rpi-flexwrap rpi-logo rpi-logo-<?php echo $biz->provider; ?>"><?php
        }     ?><div class="rpi-flex"><?php
                    if (!$opts->header_hide_photo) {
                        $img_alt = $opts->header_hide_name ? $biz->name : '';
                        $this->rp_img($biz->photo, $img_alt, $biz->provider, $opts);
                    }
                    $this->rating_info($biz, $opts); ?>
                </div>
                <?php if (!$opts->header_hide_seeall || !$opts->header_hide_write) { ?>
                <div class="rpi-slider-btn"><?php
                    if (!$opts->header_hide_seeall) $this->reviews_all($biz);
                    if (!$opts->header_hide_write) $this->review_us_on($biz);
                ?></div>
                <?php } ?>
            </div>
        </div><?php
    }

    private function reviews($revs, $opts) {
        $card_class     = '';
        $inner_class    = '';
        $triangle_class = '';
        $flex_class     = '';
        $body_class     = '';
        $is_br = $this->is_br($opts);

        $count = count($revs);
        $brb_ajax_off = get_option('brb_ajax_off');
        $reviews = $brb_ajax_off != 'true' && $count > 0 && $opts->pagination > 0 ? array_slice($revs, 0, $opts->pagination) : $revs;
        $offset = count($reviews);

        switch($opts->style_style) {
            case '1':
            case '7':
                $body_class     = ' rpi-normal-up__body';
                break;
            case '2':
            case '8':
                $body_class     = ' rpi-normal-down__body';
                break;
            case '3':
                $inner_class    = ' rpi-bubble__inner rpi-bubble__inner_up';
                $triangle_class = 'rpi-triangle rpi-triangle_up' . ($is_br ? ' rpi-triangle__br_up' : '');
                $flex_class     = ' rpi-bubble__flex rpi-bubble__flex_up';
                break;
            case '4':
                $inner_class    = ' rpi-bubble__inner rpi-bubble__inner_down';
                $triangle_class = 'rpi-triangle rpi-triangle_down' . ($is_br ? ' rpi-triangle__br_down' : '');
                $flex_class     = ' rpi-bubble__flex rpi-bubble__flex_down';
                break;
            case '5':
                $flex_class     = ' rpi-shift-up__flex';
                break;
            case '6':
                $flex_class     = ' rpi-shift-down__flex';
                break;
        }
        switch($opts->style_style) {
            case '7':
                $card_class     = ' rpi-avaborder-up';
                break;
            case '8':
                $card_class     = ' rpi-avaborder-down';
                break;
        }
        ?><div class="rpi-cards" data-count="<?php echo $count; ?>" data-offset="<?php echo $offset; ?>"><?php
        foreach ($reviews as $review) {
            $this->review($review, $opts, $card_class, $inner_class, $triangle_class, $flex_class, $body_class);
        }
        ?></div><?php
        if ($opts->view_mode != 'slider' && $opts->pagination > 0 && $count > $offset) {
        ?><div style="text-align:center">
            <a href="#" class="rpi-url"><?php echo __('More reviews', 'brb') ?></a>
        </div><?php
        }
    }

    private function review($review, $opts, $card_class = '', $inner_class = '', $triangle_class = '', $flex_class = '', $body_class = '') {
        $is_show_text = isset($review->text) && strlen($review->text) > 0;
        $is_show_reply = isset($review->reply);
        $is_show_media = isset($review->media);

        // always show a body text to make a text height (var(--text-height)) the same for each rpi-card
        $is_show_body = true; //$is_show_text || $is_show_reply || $is_show_media;
        ?>
        <div class="rpi-card<?php echo $card_class; ?>" data-provider="<?php echo $review->provider; ?>">
            <?php if ($opts->style_style == '3') { $this->review_info($review, $opts, $flex_class); } ?>
            <div class="rpi-card-inner<?php echo $inner_class; ?>">
                <i class="<?php echo $triangle_class; ?>"></i>
                <?php
                if ($opts->style_style != '3' && $opts->style_style != '4') {
                    $this->review_info($review, $opts, $flex_class);
                } ?>
                <div class="rpi-body<?php echo $body_class; ?>">
                    <?php if ($is_show_body) { ?>
                        <div class="rpi-text rpi-scroll" tabindex="0"><?php echo $review->text; ?></div>
                    <?php } ?>

                    <?php if  ($is_show_media) { ?>
                        <div class="rpi-media">
                        <?php foreach ($review->media as $media) { ?>
                            <div onclick="rpi.Utils.popup('<?php echo $media->googleUrl; ?>', 800, 600)"
                                 style="background-image:url(<?php echo str_replace('=s300', '=s' . ($this->media_size($opts)), $media->thumbnailUrl); ?>)"
                                 class="rpi-thumb rpi-clickable"></div>
                        <?php } ?>
                        </div>
                    <?php } ?>

                    <?php if  ($is_show_reply) { ?>
                    <div class="rpi-reply rpi-scroll">
                        <b><?php echo __('Response from the owner', 'brb'); ?>:</b> <?php echo $review->reply; ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <?php if ($opts->style_style == '4') { $this->review_info($review, $opts, $flex_class); } ?>
        </div>
        <?php
    }

    private function is_br($opts) {
        $br = $this->get_css_var($opts, 'card-br');
        return $br ? ($br == 'none' ? false : true) : false;
    }

    private function review_info($review, $opts, $flex_class) {
        ?><div class="rpi-flex<?php echo $flex_class; ?>">
            <?php if (isset($review->author_avatar)) { $this->author_avatar($review, $opts); } ?>
            <div class="rpi-info"><?php
                if (isset($review->author_name)) $this->review_name($review, $opts);
                if (isset($review->time)) { ?><div class="rpi-time" data-time="<?php echo $review->time; ?>"></div><?php }
                if (!$opts->hide_stars) {
                ?><div class="rpi-stars" data-info="<?php echo implode(',', array($review->rating, $review->provider, '')); ?>">
                    <?php echo $this->stars($review->rating); ?>
                </div><?php
                } ?>
            </div>
        </div><?php
    }

    private function rating_info($biz, $opts) {
        ?><div class="rpi-info"><?php
            $this->header_info($biz, $opts);
            if (!$opts->header_hide_count) { ?>
            <div class="rpi-based">
                <?php printf(esc_html__('Based on %s reviews', 'brb'), $biz->review_count); ?>
            </div>
            <?php } ?>
        </div>
        <?php
    }

    private function header_info($biz, $opts) {
        if (!$opts->header_hide_scale) { ?><div class="rpi-scale"><?php echo __($this->scale($biz->rating), 'brb'); ?></div><?php }
        if (!$opts->header_hide_name) {
            if (isset($biz->url) && strlen($biz->url) > 0) {
                $aria_label = $biz->name . ' - ' . ($biz->provider ? $biz->provider . ' ' : '') . 'business';
                $this->view_helper->anchor($biz->url, 'rpi-name', $biz->name, $opts->open_link, $opts->nofollow_link, '', '', ucfirst($aria_label));
            } else {
                ?><div class="rpi-name"><?php echo $biz->name; ?></div><?php
            }
        }
        $this->score($biz, $opts);
    }

    private function score($biz, $opts) {
        if ($biz->provider == 'yelp') {
            $yelp_rating = round($biz->rating * 2) / 2;
            $grade_class = ' rpi-grade-yelp' . ($yelp_rating * 10);
            $info_rating = $yelp_rating;
        } else {
            $grade_class = '';
            $info_rating = $biz->rating;
        }
        $data_info = implode(',', array($info_rating, $biz->provider, ''))
        ?><div class="rpi-score">
            <div class="rpi-grade<?php echo $grade_class; ?>"><?php echo $biz->rating; ?></div>
            <div class="rpi-stars" data-info="<?php echo $data_info; ?>">
                <?php echo $this->stars($biz->rating); ?>
            </div>
        </div><?php
    }

    private function review_name($review, $opts) {
        if (isset($review->author_url) && strlen($review->author_url) > 0) {
            $aria_label = $review->author_name . ' - ' . $review->provider . ' user profile';
            $this->view_helper->anchor($review->author_url, 'rpi-name', $review->author_name, $opts->open_link, $opts->nofollow_link, '', '', ucfirst($aria_label));
        } else {
            ?><div class="rpi-name"><?php echo $review->author_name; ?></div><?php
        }
    }

    private function options($opts) {
        return json_encode(
            array(
                'layout'              => $opts->view_mode,
                'style_style'         => $opts->style_style,
                'style_stars'         => $opts->style_stars,
                'style_ava'           => $opts->style_ava,
                'head_logo'           => $opts->head_logo,
                'style_logo'          => $opts->style_logo,
                'pagination'          => $opts->pagination,
                'text_size'           => $opts->text_size,
                'hide_avatar'         => $opts->hide_avatar,
                'hide_name'           => $opts->hide_name,
                'disable_review_time' => $opts->disable_review_time,
                'disable_user_link'   => $opts->disable_user_link,
                'disable_google_link' => $opts->disable_google_link,
                'open_link'           => $opts->open_link,
                'nofollow_link'       => $opts->nofollow_link,
                'lazy_load_img'       => $opts->lazy_load_img,
                'time_format'         => $opts->time_format,
                'breakpoints'         => $opts->slider_breakpoints,
                'ajax_load'           => $opts->ajax_load,
                'trans'               => array(
                    'read more'               => __('read more', 'brb'),
                    'Response from the owner' => __('Response from the owner', 'brb')
                )
            )
        );
    }

    private function tag_options($opts) {
        return json_encode(
            array(
                'tag_popup'   => $opts->tag_popup,
                'tag_click'   => $opts->tag_click,
                'tag_sidebar' => $opts->tag_sidebar
            )
        );
    }

    private function flash_options($opts) {
        return json_encode(
            array(
                'flash_start'         => $opts->flash_start     ? $opts->flash_start     : 3,
                'flash_visible'       => $opts->flash_visible   ? $opts->flash_visible   : 5,
                'flash_invisible'     => $opts->flash_invisible ? $opts->flash_invisible : 5,
                'flash_user_photo'    => $opts->flash_user_photo,
                'flash_hide_logo'     => $opts->flash_hide_logo,
                'hide_avatar'         => $opts->hide_avatar,
                'hide_name'           => $opts->hide_name,
                'disable_review_time' => $opts->disable_review_time,
                'time_format'         => $opts->time_format,
                'lazy_load_img'       => $opts->lazy_load_img,
                'flash_head'          => $opts->flash_head,
                'flash_body'          => $opts->flash_body,
                'flash_footer'        => $opts->flash_footer,
                //'flash_width'         => $opts->flash_width,
                'text' => array(
                    'm1' => __('just left us a %s review', 'brb'),
                    'm2' => __('on', 'brb')
                )
            )
        );
    }

    private function slider_options($opts) {
        return json_encode(
            array(
                'pagination'    => $opts->pagination,
                'speed'         => $opts->slider_speed ? $opts->slider_speed : 5,
                'autoplay'      => $opts->slider_autoplay,
                'wheelscroll'   => $opts->slider_wheelscroll,
                'mousestop'     => $opts->slider_mousestop,
                'clickstop'     => $opts->slider_clickstop,
                'swipe_step'    => $opts->slider_swipe_step,
                'swipe_per_btn' => $opts->slider_swipe_per_btn,
                'swipe_per_dot' => $opts->slider_swipe_per_dot,
                'hide_dots'     => $opts->slider_hide_pagin
            )
        );
    }

    private function scale($rating) {
        if ($rating > 4.2) {
            return 'Excellent';
        } elseif ($rating > 3.7) {
            return 'Great';
        } elseif ($rating > 2.7) {
            return 'Good';
        } elseif ($rating > 1.7) {
            return 'Fair';
        } else {
            return 'Poor';
        }
    }

    private function logos($obj) {
        ?><span class="rpi-logos"><?php
        foreach ($obj->platform as $p) {
            ?><span class="rpi-logo rpi-logo-<?php echo $p; ?>"></span><?php
        }
        ?></span><?php
    }

    private function reviews_all($biz) {
        if ($biz->id == 'summary' && strlen($biz->wr) > 0) {
            $pair = explode(':', $biz->wr);
            $biz->provider = $pair[0];
            $biz->id = $pair[1];
        }
        ?>
        <div class="rpi-review_us rpi-clickable">
            <a href="<?php echo $this->get_allreview_url($biz); ?>" target="_blank" rel="noopener"><?php echo __('See all reviews', 'brb'); ?></a>
        </div>
        <?php
    }

    private function review_us_on($biz) {
        if ($biz->id == 'summary' && strlen($biz->wr) > 0) {
            $pair = explode(':', $biz->wr);
            $biz->provider = $pair[0];
            $biz->id = $pair[1];
        }
        ?>
        <div class="rpi-review_us rpi-clickable">
            <a href="<?php echo $this->get_writereview_url($biz); ?>" class="rpi-logo_after rpi-logo-<?php echo $biz->provider; ?>" target="_blank" rel="noopener" onclick="rpi.Utils.popup('<?php echo $this->get_writereview_url($biz); ?>', 800, 600)"><?php echo __('review us on', 'brb'); ?></a>
        </div>
        <?php
    }

    private function get_writereview_url($biz) {
        $id = $biz->id;
        switch ($biz->provider) {
            case 'google':
                return 'https://search.google.com/local/writereview?placeid=' . $id;
            case 'facebook':
                return 'https://facebook.com/' . $id . '/reviews';
            case 'yelp':
                return 'https://www.yelp.com/writeareview/biz/' . $id;
        }
    }

    private function get_allreview_url($biz) {
        if ($biz->id == 'summary' && strlen($biz->wr) > 0) {
            $pair = explode(':', $biz->wr);
            $biz->provider = $pair[0];
            $biz->id = $pair[1];
        }
        switch ($biz->provider) {
            case 'google':
                return 'https://search.google.com/local/reviews?placeid=' . $biz->id;
            case 'facebook':
                return 'https://facebook.com/' . $biz->id . '/reviews';
            case 'yelp':
                return $biz->url;
        }
    }

    private function stars($rating) {
        $stars = '';
        for ($i = 0; $i < 5; $i++) {
            $score = $rating - $i;
            if ($score <= 0) {
                $stars .= '<span class="rpi-star rpi-star-o"></span>';
            } elseif ($score > 0 && $score < 1) {
                if ($score < 0.25) {
                    $stars .= '<span class="rpi-star rpi-star-o"></span>';
                } elseif ($score > 0.75) {
                    $stars .= '<span class="rpi-star"></span>';
                } else {
                    $stars .= '<span class="rpi-star rpi-star-h"></span>';
                }
            } else {
                $stars .= '<span class="rpi-star"></span>';
            }
        }
        return $stars;
    }

    public function author_avatar($review, $opts) {
        switch ($review->provider) {
            case 'google':
                $def_alt = 'Google user';
                $def_ava = BRB_GOOGLE_AVATAR;
                break;
            case 'facebook':
                $def_alt = 'Facebook user';
                $def_ava = BRB_FACEBOOK_AVATAR;
                break;
            case 'yelp':
                $def_alt = 'Yelp user';
                $def_ava = BRB_YELP_AVATAR;
                break;
        }
        $alt = $opts->hide_name && isset($review->author_name) ? $review->author_name : '';
        $this->rp_img($review->author_avatar, $alt, $review->provider, $opts, $def_ava);
    }

    public function rp_img($src, $alt, $platform, $opts, $def_ava = BRB_DEFAULT_AVATAR) {
        $img_size = $this->img_size($opts);
        switch ($platform) {
            case 'google':
                $regexp = '/googleusercontent\.com\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)\/photo\.jpg/';
                preg_match($regexp, $src, $matches, PREG_OFFSET_CAPTURE);
                if (count($matches) > 4 && $matches[3][0] == 'AAAAAAAAAAA') {
                    $src = str_replace('/photo.jpg', '/s128-c0x00000000-cc-rp-mo/photo.jpg', $src);
                }
                if (strlen($src) > 0) {
                    if (strpos($src, "s128") != false) {
                        $src = str_replace('s128', 's' . $img_size, $src);
                    } elseif (strpos($src, "-mo") != false) {
                        $src = str_replace('-mo', '-mo-s' . $img_size, $src);
                    } else {
                        $src = str_replace('-rp', '-rp-s' . $img_size, $src);
                    }
                }
                break;
            case 'yelp':
                if (strlen($src) > 0) {
                    $avatar_size = '';
                    if ($img_size <= 128) {
                        $avatar_size = 'ms';
                    } else {
                        $avatar_size = 'o';
                    }
                    $src = preg_replace('/(ms)|(o)\.jpg/', $avatar_size, $src);
                }
                break;
        }
        $src = strlen($src) > 0 ? $src : $def_ava;
        ?><div class="rpi-img"><?php $this->img($src, $alt, $opts, $def_ava); ?></div><?php
    }

    public function img($src, $alt, $opts, $def_ava = BRB_DEFAULT_AVATAR) {
        $size = $this->img_size($opts);
        ?><img src="<?php echo $src; ?>" class="rpi-img" <?php if ($opts->lazy_load_img) { ?>loading="lazy"<?php } ?> width="<?php echo $size; ?>" height="<?php echo $size; ?>" alt="<?php echo $alt; ?>" onerror="if(this.src!='<?php echo $def_ava; ?>')this.src='<?php echo $def_ava; ?>';"><?php
    }

    public function img_size($opts) {
        $size = $this->get_css_var($opts, 'img-size');
        return $size ? $size : BRB_AVATAR_SIZE;
    }

    public function media_size($opts) {
        $size = $this->get_css_var($opts, 'media-lines');
        return ($size ? $size : 2) * 22;
    }

    public function get_css_var($opts, $name) {
        return preg_match('/--' . $name . ':\s?(\d+)/', $opts->style_vars, $match) && count($match) > 1 ? $match[1] : null;
    }
}
