<?php

namespace WP_Business_Reviews_Bundle\Includes;

use WP_Business_Reviews_Bundle\Includes\Core\Database;

class Plugin_Overview {

    private $helper;

    public function __construct(Helper $helper) {
        $this->helper = $helper;
    }

    public function register() {
        add_action('brb_admin_page_brb', array($this, 'init'));
        add_action('brb_admin_page_brb', array($this, 'render'));
    }

    public function init() {

    }

    public function render() {
        global $wpdb;

        $auth_code = get_option('brb_auth_code');
        $auth_code_test = get_option('brb_auth_code_test');
        $auth_code = isset($auth_code_test) && strlen($auth_code_test) > 0 ? $auth_code_test : $auth_code;

        $connects = $wpdb->get_results(
            'SELECT conn_id, conn_name, platform FROM ' . $wpdb->prefix . Database::CONNECT_TABLE .
            ' WHERE platform = "google" GROUP BY conn_id ORDER BY MAX(coll_id) DESC;'
        );

        $locations = [];
        foreach ($connects as $conn) {
            $locations[$conn->conn_id] = $conn->conn_name;
        }

        wp_nonce_field('brb_wpnonce', 'brb_nonce');
        ?>

        <div class="rpi-overview"></div>

        <script>
            const accounts = [];
            const locations = {};
            const locstemp = <?php echo json_encode($locations); ?>;
            let firstId = null;

            for (let l in locstemp) {
                let pair = /accounts\/(\d+)\/locations\/(\d+)/.exec(l),
                    name = 'locations/' + pair[2];
                locations[name] = locations[name] || {account: [], location: {}};
                locations[name].account.push({id: pair[1]});
                locations[name].location.name = name;
                locations[name].location.title = locstemp[l];
                accounts.push(pair[1]);
                if (!firstId) firstId = l;
            }

            const stats = rpi.Stats(document.getElementsByClassName('rpi-overview')[0], {
                id: firstId,
                accounts: accounts.join(','),
                locations: locations,
                withrevs: true,
                sbwidth: 384,
                hash: '<?php echo md5($auth_code); ?>'
            });
            stats.init();
        </script>
        <?php
    }
}
