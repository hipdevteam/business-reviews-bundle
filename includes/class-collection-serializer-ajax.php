<?php

namespace WP_Business_Reviews_Bundle\Includes;

use WP_Business_Reviews_Bundle\Includes\Core\Core;
use WP_Business_Reviews_Bundle\Includes\View\View;

class Collection_Serializer_Ajax {

    private $core;
    private $view;
    private $serializer;
    private $deserializer;

    public function __construct(Collection_Serializer $serializer, Collection_Deserializer $deserializer, Core $core, View $view) {
        $this->serializer = $serializer;
        $this->deserializer = $deserializer;
        $this->core = $core;
        $this->view = $view;

        add_action('wp_ajax_brb_collection_save_ajax', array($this, 'save_ajax'));
        add_action('wp_ajax_brb_apply_all_opt', array($this, 'apply_all_opt'));
    }

    public function save_ajax() {

        $post_id = $this->serializer->save($_POST['post_id'], $_POST['title'], $_POST['content']);

        if (isset($post_id)) {
            $collection = $this->deserializer->get_collection($post_id);

            $data = $this->core->get_reviews($collection);
            $businesses = $data['businesses'];
            $reviews = $data['reviews'];
            $options = $data['options'];

            wp_send_json(['html' => $this->view->render($collection->ID, $businesses, $reviews, $options), 'errors' => $data['errors']]);
        }

        wp_die();
    }

    public function apply_all_opt() {

        $applied_coll_ids = [];

        $collection = $this->deserializer->get_collection($_POST['post_id']);
        $collection_json = json_decode($collection->post_content);

        $wp_query = new \WP_Query();

        $wp_query->query(array(
            'post_type'      => 'brb_collection',
            'fields'         => array('ID', 'post_title', 'post_content'),
            'posts_per_page' => 300,
            'no_found_rows'  => true,
        ));
        $collections = $wp_query->posts;

        foreach ($collections as $coll) {
            $conn = json_decode($coll->post_content);
            if (isset($conn->options) && $conn->options->view_mode == $_POST['theme']) {
                $conn->options = $collection_json->options;
                $post_id = wp_insert_post(array(
                    'ID'           => $coll->ID,
                    'post_title'   => $coll->post_title,
                    'post_content' => json_encode($conn, JSON_UNESCAPED_UNICODE),
                    'post_type'    => 'brb_collection',
                    'post_status'  => 'publish',
                ));
                array_push($applied_coll_ids, $coll->ID);
            }
        }
        wp_send_json(['status' => 'success', 'ids' => $applied_coll_ids]);
        wp_die();
    }

}
