<?php

namespace WP_Business_Reviews_Bundle\Includes;

use WP_Business_Reviews_Bundle\Includes\Core\Database;

class Collection_Connect {

    public function recreate($coll_id, $conns) {
        global $wpdb;

        $rows = array();
        foreach ($conns as $conn) {
            array_push($rows, array(
                'coll_id'  => $coll_id,
                'platform' => $conn->platform,
                'conn_id'  => $conn->id,
                'conn_name'  => $conn->name
            ));
        }

        $this->delete_all($coll_id);
        $this->insert_bulk($wpdb->prefix . Database::CONNECT_TABLE, $rows);
    }

    public function delete_all($coll_id) {
        global $wpdb;

        $wpdb->delete($wpdb->prefix . Database::CONNECT_TABLE, array('coll_id' => $coll_id));
    }

    private function insert_bulk($table, $rows) {
        global $wpdb;

        $columns = array_keys($rows[0]);
        asort($columns);
        $columnList = '`' . implode('`, `', $columns) . '`';

        $sql = "INSERT INTO `$table` ($columnList) VALUES\n";
        $placeholders = array();
        $data = array();

        foreach ($rows as $row) {
            ksort($row);
            $rowPlaceholders = array();
            foreach ($row as $key => $value) {
                $data[] = $value;
                $rowPlaceholders[] = is_numeric($value) ? '%d' : '%s';
            }
            $placeholders[] = '(' . implode(', ', $rowPlaceholders) . ')';
        }

        $sql .= implode(",\n", $placeholders);
        return $wpdb->query($wpdb->prepare($sql, $data));
    }
}
