<?php

namespace WP_Business_Reviews_Bundle\Includes;

class Cache_Cleaner {

    public function __construct() {
    }

    public function purgeAll(array $postIds = []) {
        $this->purgeEndurance($postIds);
        $this->purgeFlyingPress($postIds);
        $this->purgeHummingbird($postIds);
        $this->purgeLitespeed($postIds);
        $this->purgeNitropack($postIds);
        $this->purgeSiteGround($postIds);
        $this->purgeSwiftPerformance($postIds);
        $this->purgeW3Total($postIds);
        $this->purgeWPFastest($postIds);
        $this->purgeWPOptimize($postIds);
        $this->purgeWPRocket($postIds);
        $this->purgeWPSuper($postIds);
    }

    protected function purgeEndurance(array $postIds = []) {
        do_action('epc_purge');
    }

    protected function purgeFlyingPress(array $postIds = []) {
        if (!class_exists('FlyingPress\Purge')) {
            return;
        }
        if (is_callable(['FlyingPress\Purge', 'purge_pages']) && empty($postIds)) {
            \FlyingPress\Purge::purge_pages();
        } elseif (is_callable(['FlyingPress\Purge', 'purge_url'])) {
            foreach ($postIds as $postId) {
                \FlyingPress\Purge::purge_url(get_permalink($postId));
            }
        }
    }

    protected function purgeHummingbird(array $postIds = []) {
        if (empty($postIds)) {
            do_action('wphb_clear_page_cache');
        }
        foreach ($postIds as $postId) {
            do_action('wphb_clear_page_cache', $postId);
        }
    }

    protected function purgeLitespeed(array $postIds = []) {
        if (empty($postIds)) {
            do_action('litespeed_purge_all');
        }
        foreach ($postIds as $postId) {
            do_action('litespeed_purge_post', $postId);
        }
    }

    protected function purgeNitropack(array $postIds = []) {
        if (!function_exists('nitropack_invalidate') || !function_exists('nitropack_get_cacheable_object_types')) {
            return;
        }
        if (!get_option('nitropack-autoCachePurge', 1)) {
            return;
        }
        if (empty($postIds)) {
            nitropack_invalidate(null, null, 'Invalidating all pages');
            return;
        }
        foreach ($postIds as $postId) {
            $cacheableTypes = nitropack_get_cacheable_object_types();
            $post = get_post($postId);
            $postType = $post->post_type ?? 'post';
            $postTitle = $post->post_title ?? '';
            if (in_array($postType, $cacheableTypes)) {
                nitropack_invalidate(null, "single:{$postId}", sprintf('Invalidating "%s" page', $postTitle));
            }
        }
    }

    protected function purgeSiteGround(array $postIds = []) {
        if (function_exists('sg_cachepress_purge_everything') && empty($postIds)) {
            sg_cachepress_purge_everything();
        }
        if (function_exists('sg_cachepress_purge_cache')) {
            foreach ($postIds as $postId) {
                sg_cachepress_purge_cache(get_permalink($postId));
            }
        }
    }

    protected function purgeSwiftPerformance(array $postIds = []) {
        if (!class_exists('Swift_Performance_Cache')) {
            return;
        }
        if (empty($postIds)) {
            \Swift_Performance_Cache::clear_all_cache();
        } else {
            \Swift_Performance_Cache::clear_post_cache_array($postIds);
        }
    }

    protected function purgeW3Total(array $postIds = []) {
        if (empty($postIds)) {
            if (function_exists('w3tc_flush_all')) {
                w3tc_flush_all();
            }
            if (class_exists('W3_Plugin_TotalCacheAdmin')) {
                $plugin_totalcacheadmin = & w3_instance('W3_Plugin_TotalCacheAdmin');
                $plugin_totalcacheadmin->flush_all();
            }
        } else {
            if (function_exists('w3tc_flush_post')) {
                foreach ($postIds as $postId) {
                    w3tc_flush_post($postId);
                }
            }
        }
    }

    protected function purgeWPFastest(array $postIds = []) {
        if (empty($postIds)) {
            do_action('wpfc_clear_all_cache');
            if (isset($GLOBALS['wp_fastest_cache']) && method_exists($GLOBALS['wp_fastest_cache'], 'deleteCache')) {
                $GLOBALS['wp_fastest_cache']->deleteCache();
            }
        } else {
            foreach ($postIds as $postId) {
                do_action('wpfc_clear_post_cache_by_id', false, $postId);
            }
        }
    }

    protected function purgeWPOptimize(array $postIds = []) {
        if (function_exists('WP_Optimize') && empty($postIds)) {
            WP_Optimize()->get_page_cache()->purge();
        }
        if (class_exists('WPO_Page_Cache')) {
            foreach ($postIds as $postId) {
                \WPO_Page_Cache::delete_single_post_cache($postId);
            }
        }
    }

    protected function purgeWPRocket(array $postIds = []) {
        if (empty($postIds)) {
            if (function_exists('rocket_clean_home')) {
                rocket_clean_home();
            }
            if (function_exists('rocket_clean_domain')) {
                rocket_clean_domain();
            }
        } else {
            if (function_exists('rocket_clean_post')) {
                foreach ($postIds as $postId) {
                    rocket_clean_post($postId);
                }
            }
        }
    }

    protected function purgeWPSuper(array $postIds = []) {
        if (empty($postIds)) {
            if (function_exists('wp_cache_clear_cache')) {
                wp_cache_clear_cache();
            }
        } else {
            if (function_exists('wp_cache_post_change')) {
                foreach ($postIds as $postId) {
                    wp_cache_post_change($postId);
                }
            }
        }
    }
}
