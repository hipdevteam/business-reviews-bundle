<?php

namespace WP_Business_Reviews_Bundle\Includes;

class Assets {

    private $url;
    private $version;
    private $debug_js;
    private $debug_css;

    private static $css_assets = array(
        'brb-admin-main-css'     => 'css/admin-main',
        'brb-admin-overview-css' => 'css/admin-overview',
        'brb-public-main-css'    => 'css/public-main',
        'brb-public-main1-css'   => 'css/public-main1',
        'brb-public-swiper-css'  => 'css/public-swiper.min',
        'brb-public-rplg-css'    => 'css/public-richplugins',

        'rpi-common-css'         => 'https://cdn.reviewsplugin.com/assets/css/common.css',
        'rpi-column-css'         => 'https://cdn.reviewsplugin.com/assets/css/column.css',
        'rpi-stats-css'          => 'https://cdn.reviewsplugin.com/assets/css/stat.css',
        'rpi-flash-css'          => 'https://cdn.reviewsplugin.com/assets/css/flash.css'
    );

    private static $js_assets = array(
        'brb-admin-main-js'     => 'js/admin-main',
        'brb-admin-builder-js'  => 'js/admin-builder',
        'brb-admin-overview-js' => 'js/admin-overview',

        'brb-public-swiper-js'  => 'js/public-swiper.min',
        'brb-public-main-js'    => 'js/public-main',
        'brb-public-main1-js'   => 'js/public-main1',
        'brb-public-rplg-js'    => 'js/public-richplugins',

        'rpi-url-js'            => 'https://cdn.reviewsplugin.com/assets/js/url.js',
        'rpi-stats-js'          => 'https://cdn.reviewsplugin.com/assets/js/stats.js',
        'rpi-time-js'           => 'https://cdn.reviewsplugin.com/assets/js/time.js',
        'rpi-utils-js'          => 'https://cdn.reviewsplugin.com/assets/js/utils.js',
        'rpi-column-js'         => 'https://cdn.reviewsplugin.com/assets/js/column.js',
        'rpi-common-js'         => 'https://cdn.reviewsplugin.com/assets/js/common.js',
        'rpi-view-js'           => 'https://cdn.reviewsplugin.com/assets/js/view.js',
        'rpi-slider-js'         => 'https://cdn.reviewsplugin.com/assets/js/slider.js',
        'rpi-flash-js'          => 'https://cdn.reviewsplugin.com/assets/js/flash.js',

        'brb-moment-js'                    => 'https://cdn.reviewsplugin.com/assets/js/lib/moment.min.js',
        'brb-chart-js'                     => 'https://cdn.reviewsplugin.com/assets/js/lib/chart.js',
        'brb-chartjs-adapter-moment-js'    => 'https://cdn.reviewsplugin.com/assets/js/lib/chartjs-adapter-moment.min.js',
        'brb-chartjs-plugin-datalabels-js' => 'https://cdn.reviewsplugin.com/assets/js/lib/chartjs-plugin-datalabels.min.js'
    );

    public function __construct($url, $version, $debug) {
        $this->url       = $url;
        $this->version   = $version;
        $this->debug_js  = $debug;
        $this->debug_css = $debug;
    }

    public function register() {
        if (is_admin()) {
            add_action('admin_enqueue_scripts', array($this, 'register_styles'));
            add_action('admin_enqueue_scripts', array($this, 'register_scripts'));
            add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_styles'));
            add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_scripts'));
        } else {
            add_action('wp_enqueue_scripts', array($this, 'register_styles'));
            add_action('wp_enqueue_scripts', array($this, 'register_scripts'));

            $brb_demand_assets = get_option('brb_demand_assets');
            if (!$brb_demand_assets || $brb_demand_assets != 'true') {
                add_action('wp_enqueue_scripts', array($this, 'enqueue_public_styles'));
                add_action('wp_enqueue_scripts', array($this, 'enqueue_public_scripts'));
            }

            add_filter('script_loader_tag', array($this, 'script_async'), 10, 2);

            $async_css = get_option('brb_async_css');
            if ($async_css === 'true') {
                add_filter('style_loader_tag', array($this, 'style_async'), 10, 2);
            }
        }
        add_filter('get_rocket_option_remove_unused_css_safelist', array($this, 'rucss_safelist'));
    }

    function script_async($tag, $handle) {
        $js_assets = array(
            'brb-admin-main-js'     => 'js/admin-main',
            'brb-admin-builder-js'  => 'js/admin-builder',
            'brb-admin-overview-js' => 'js/admin-overview',
            'brb-public-swiper-js'  => 'js/public-swiper.min',
            'brb-public-rplg-js'    => 'js/public-richplugins',
            'brb-public-main-js'    => 'js/public-main',
        );
        if (isset($handle) && array_key_exists($handle, $js_assets)) {
            return str_replace(' src', ' defer="defer" src', $tag);
        }
        return $tag;
    }

    function style_async($tag, $handle) {
        $css_assets = array(
            'brb-admin-main-css'     => 'css/admin-main',
            'brb-admin-overview-css' => 'css/admin-overview',
            'brb-public-main-css'    => 'css/public-main',
            'brb-public-swiper-css'  => 'css/public-swiper.min',
            'brb-public-rplg-css'    => 'css/public-richplugins',
        );
        if (isset($handle) && array_key_exists($handle, $css_assets)) {
            return str_replace(" rel='stylesheet'", " rel='preload' as='style' onload='this.onload=null;this.rel=\"stylesheet\";window.dispatchEvent(new Event(\"resize\"))'", $tag);
        }
        return $tag;
    }

    function rucss_safelist($safelist) {
        $css_main = $this->get_css_asset('brb-public-main-css');
        $css_swiper = $this->get_css_asset('brb-public-swiper-css');
        if (array_search($css_main, $safelist) === false) {
            $safelist[] = $css_main;
        }
        if (array_search($css_swiper, $safelist) === false) {
            $safelist[] = $css_swiper;
        }
        return $safelist;
    }

    public function register_styles() {
        $styles = array(
            'brb-admin-main-css',
            'brb-admin-overview-css',
            'brb-public-main-css',
            'brb-public-main1-css',
            'brb-public-rplg-css',
            'brb-public-swiper-css',
            'rpi-common-css',
            'rpi-column-css',
            'rpi-stats-css',
            'rpi-flash-css'
        );
        $this->register_styles_loop($styles);
    }

    public function register_scripts() {
        $scripts = array(
            'brb-admin-main-js',
            'brb-admin-overview-js',
            'brb-public-main-js',

            // scripts for supporting OLD versions of the plugin
            'brb-public-main1-js',
            'brb-public-rplg-js',
            'brb-public-swiper-js'
        );
        if ($this->debug_js) {
            array_push($scripts, 'brb-admin-builder-js');
            array_push($scripts, 'rpi-url-js');
            array_push($scripts, 'rpi-stats-js');
            array_push($scripts, 'rpi-time-js');
            array_push($scripts, 'rpi-utils-js');
            array_push($scripts, 'rpi-column-js');
            array_push($scripts, 'rpi-common-js');
            array_push($scripts, 'rpi-view-js');
            array_push($scripts, 'rpi-slider-js');
            array_push($scripts, 'rpi-flash-js');

            array_push($scripts, 'brb-moment-js');
            array_push($scripts, 'brb-chart-js');
            array_push($scripts, 'brb-chartjs-adapter-moment-js');
            array_push($scripts, 'brb-chartjs-plugin-datalabels-js');
        }
        $this->register_scripts_loop($scripts);
    }

    public function enqueue_admin_styles() {
        wp_enqueue_style('brb-admin-main-css');
        wp_style_add_data('brb-admin-main-css', 'rtl', 'replace');

        if (isset($_GET['page'])) {
            switch ($_GET['page']) {
                case 'brb':
                    $this->enqueue_admin_overview_styles();
                    break;
            }
        }

        /* Load swiper js coz it doesn't loaded by ajax request */
        wp_enqueue_style('brb-public-swiper-css');

        $this->enqueue_public_styles();
    }

    public function enqueue_admin_overview_styles() {
        if ($this->debug_css) {
            wp_enqueue_style('rpi-column-css');
            wp_enqueue_style('rpi-stats-css');
        } else {
            wp_enqueue_style('brb-admin-overview-css');
            wp_style_add_data('brb-admin-overview-css', 'rtl', 'replace');
        }
    }

    public function enqueue_admin_scripts() {
        wp_enqueue_script('jquery');

        if (isset($_GET['page'])) {
            switch ($_GET['page']) {
                case 'brb':
                    $this->enqueue_admin_overview_scripts();
                    break;
                case 'brb-builder':
                    wp_enqueue_script('jquery-ui-core');
                    wp_enqueue_script('jquery-ui-draggable');
                    wp_enqueue_script('jquery-ui-sortable');
                    break;
            }
        }

        $vars = array(
            'wordpress'      => true,
            'googleAPIKey'   => get_option('brb_google_api_key'),
            'collectionUrl'  => admin_url('admin.php?page=brb-builder'),
            'settingsUrl'    => admin_url('admin.php?page=brb-settings'),
            'BRB_ASSETS_URL' => BRB_ASSETS_URL
        );

        if ($this->debug_js) {
            wp_localize_script('brb-admin-builder-js', 'BRB_VARS', $vars);
            wp_enqueue_script('brb-admin-builder-js');
        } else {
            wp_localize_script('brb-admin-main-js', 'BRB_VARS', $vars);
        }
        wp_enqueue_script('brb-admin-main-js');

        /* Load swiper js coz it doesn't loaded by ajax request */
        wp_enqueue_script('brb-public-swiper-js');

        $this->enqueue_public_scripts();
    }

    public function enqueue_admin_overview_scripts() {
        if ($this->debug_js) {
            wp_enqueue_script('brb-moment-js');
            wp_enqueue_script('brb-chart-js');
            wp_enqueue_script('brb-chartjs-adapter-moment-js');
            wp_enqueue_script('brb-chartjs-plugin-datalabels-js');
            wp_enqueue_script('rpi-url-js');
            wp_enqueue_script('rpi-stats-js');
        } else {
            wp_enqueue_script('brb-admin-overview-js');
        }
    }

    public function enqueue_public_styles() {
        $brb_nocss = get_option('brb_nocss');
        if ($brb_nocss != 'true') {
            if ($this->debug_css) {
                wp_enqueue_style('brb-public-rplg-css');
                wp_enqueue_style('rpi-common-css');
                wp_enqueue_style('rpi-flash-css');
            }
            wp_enqueue_style('brb-public-main-css');
            wp_style_add_data('brb-public-main-css', 'rtl', 'replace');

            // Old slider_lite and tag themes
            $layout_old = get_option('brb_layout_old');
            if ($layout_old == '1') {
                wp_enqueue_style('brb-public-main1-css');
            }
        }
    }

    public function enqueue_public_scripts() {
        $vars = array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'gavatar' => BRB_GOOGLE_AVATAR,
        );

        if ($this->debug_js) {
            wp_enqueue_script('rpi-time-js');
            wp_enqueue_script('rpi-utils-js');
            wp_enqueue_script('rpi-column-js');
            wp_enqueue_script('rpi-common-js');
            wp_enqueue_script('rpi-view-js');
            wp_enqueue_script('rpi-slider-js');
            wp_enqueue_script('rpi-flash-js');
            wp_localize_script('brb-public-rplg-js', 'brb_vars', $vars);
            wp_enqueue_script('brb-public-rplg-js');
        }

        wp_localize_script('brb-public-main-js', 'brb_vars', $vars);
        wp_enqueue_script('brb-public-main-js');

        // Old slider_lite and tag themes
        $layout_old = get_option('brb_layout_old');
        if ($layout_old == '1') {
            wp_localize_script('brb-public-main1-js', 'brb_vars', $vars);
            wp_enqueue_script('brb-public-main1-js');
        }
    }

    public function get_public_styles() {
        $assets = array();
        if ($this->debug_css) {
            array_push($assets, $this->get_css_asset('brb-public-swiper-css'));
        }
        array_push($assets, $this->get_css_asset('brb-public-main-css'));
        return $assets;
    }

    public function get_public_scripts() {
        $assets = array();
        if ($this->debug_js) {
            array_push($assets, $this->get_js_asset('rpi-time-js'));
            array_push($assets, $this->get_js_asset('rpi-utils-js'));
            array_push($assets, $this->get_js_asset('rpi-column-js'));
            array_push($assets, $this->get_js_asset('rpi-common-js'));
            array_push($assets, $this->get_js_asset('rpi-view-js'));
            array_push($assets, $this->get_js_asset('rpi-slider-js'));
            array_push($assets, $this->get_js_asset('rpi-flash-js'));
            array_push($assets, $this->get_js_asset('brb-public-swiper-js'));
        }
        array_push($assets, $this->get_js_asset('brb-public-main-js'));
        return $assets;
    }

    private function register_styles_loop($styles) {
        foreach ($styles as $style) {
            wp_register_style($style, $this->get_css_asset($style), array(), $this->version);
        }
    }

    private function register_scripts_loop($scripts) {
        foreach ($scripts as $script) {
            wp_register_script($script, $this->get_js_asset($script), array(), $this->version);
        }
    }

    private function get_css_asset($asset) {
        $css = self::$css_assets[$asset];
        return strpos($css, 'https:') === 0 ? $css : $this->url . ($this->debug_css ? 'src/' : '') . $css . '.css';
    }

    private function get_js_asset($asset) {
        $js = self::$js_assets[$asset];
        return strpos($js, 'https:') === 0 ? $js : $this->url . ($this->debug_js ? 'src/' : '') . $js . '.js';
    }

}