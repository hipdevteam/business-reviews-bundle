<?php

namespace WP_Business_Reviews_Bundle\Includes\Core;

use WP_Business_Reviews_Bundle\Includes\Helper;

class Connect_Yelp {

    const YELP_API_URL = 'https://api.yelp.com/v3/businesses/';

    private $helper;

    public function __construct(Helper $helper) {
        $this->helper = $helper;

        add_action('brb_yelp_refresh', array($this, 'brb_yelp_refresh'));
        add_action('wp_ajax_brb_connect_yelp', array($this, 'connect_yelp'));
    }

    public function connect_yelp() {
        if (current_user_can('manage_options')) {
            if (isset($_POST['brb_wpnonce']) === false) {
                $error = __('Unable to call request. Make sure you are accessing this page from the Wordpress dashboard.');
                $response = compact('error');
            } else {
                check_admin_referer('brb_wpnonce', 'brb_wpnonce');

                $id = sanitize_text_field(wp_unslash($_POST['id']));
                $lang = sanitize_text_field(wp_unslash($_POST['lang']));
                //$local_img = sanitize_text_field(wp_unslash($_POST['local_img']));

                $body_json = $this->get_yelp_reviews($id, $lang);

                if ($body_json && empty($body_json->error_message)) {
                    $this->save_reviews($body_json, $lang);
                    $result = array(
                        'id'      => $body_json->id,
                        'name'    => $body_json->name,
                        'photo'   => strlen($body_json->photo) ? $body_json->photo : BRB_BIZ_LOGO,
                        'reviews' => $body_json->reviews
                    );
                    $status = 'success';
                } else {
                    $result = $body_json;
                    $status = 'error';
                }
                $response = compact('status', 'result');
            }
            header('Content-type: text/javascript');
            echo json_encode($response);
            die();
        }
    }

    function brb_yelp_refresh($args) {
        $id = $args[0];
        $lang = $args[1];

        $body_json = $this->get_yelp_reviews($id, $lang);

        if ($body_json && empty($body_json->error_message)) {
            $this->save_reviews($body_json, $lang);
            delete_transient('brb_yelp_refresh_' . join('_', $args));
        }
    }

    function get_yelp_reviews($id, $lang) {
        $yelp_api_key = get_option('brb_yelp_api_key');

        if ($yelp_api_key && strlen($yelp_api_key) > 0) {

            $yelp_biz_url = self::YELP_API_URL . $id;
            $auth_header = array('Authorization' => 'Bearer ' . $yelp_api_key);

            $body_json = $this->helper->json_remote_get($yelp_biz_url, $auth_header);

            $yelp_revs_url = $yelp_biz_url . '/reviews';
            if (strlen($lang) > 0) {
                $yelp_revs_url = $yelp_revs_url . '?locale=' . $lang;
            }

            $body_json_reviews = $this->helper->json_remote_get($yelp_revs_url, $auth_header);

            $body_json->photo = $body_json->image_url;
            $body_json->reviews = $body_json_reviews->reviews;

        } else {

            $url = BRB_YELP_API . '/get/json' .
                   '?siteurl=' . get_option('siteurl') .
                   '&authcode=' . get_option('brb_auth_code') .
                   '&id=' . $id;
            if ($lang && strlen($lang) > 0) {
                $url = $url . '&lang=' . $lang;
            }

            $res = wp_remote_get($url);
            $body = wp_remote_retrieve_body($res);
            $body_json = json_decode($body);
        }
        return $body_json;
    }

    function save_reviews($json, $lang = null) {
        global $wpdb;

        $business_id = $wpdb->get_var(
                           $wpdb->prepare(
                               "SELECT id FROM " . $wpdb->prefix . Database::BUSINESS_TABLE . " WHERE place_id = %s AND platform = %s",
                               $json->id, 'yelp'
                           )
                       );

        if ($business_id) {

            $wpdb->update($wpdb->prefix . Database::BUSINESS_TABLE, array(
                'name'         => $json->name,
                'photo'        => $json->photo,
                'rating'       => $json->rating,
                'review_count' => $json->review_count
            ), array('ID' => $business_id));

        } else {

            $address = implode(", ", array(
                $json->location->address1,
                $json->location->city,
                $json->location->state,
                $json->location->zip_code
            ));
            $wpdb->insert($wpdb->prefix . Database::BUSINESS_TABLE, array(
                'place_id'     => $json->id,
                'name'         => $json->name,
                'photo'        => $json->photo,
                'address'      => $address,
                'rating'       => $json->rating,
                'url'          => $json->url,
                'review_count' => $json->review_count,
                'platform'     => 'yelp'
            ));
            $business_id = $wpdb->insert_id;

        }

        if ($json->reviews) {
            foreach ($json->reviews as $review) {

                $review_id = $wpdb->get_var($wpdb->prepare("SELECT id FROM " . $wpdb->prefix . Database::REVIEW_TABLE . " WHERE review_id = %s AND business_id = %d AND platform = %s", $review->id, $business_id, 'yelp'));
                if ($review_id) {

                    $wpdb->update($wpdb->prefix . Database::REVIEW_TABLE, array(
                        'rating'      => $review->rating,
                        'text'        => $review->text,
                        'author_name' => $review->user->name,
                        'author_img'  => $review->user->image_url
                    ), array('ID' => $review_id));

                } else {

                    $wpdb->insert($wpdb->prefix . Database::REVIEW_TABLE, array(
                        'business_id' => $business_id,
                        'review_id'   => $review->id,
                        'rating'      => $review->rating,
                        'text'        => $review->text,
                        'url'         => $review->url,
                        'language'    => $lang,
                        'time_str'    => $review->time_created,
                        'author_name' => $review->user->name,
                        'author_img'  => $review->user->image_url,
                        'platform'    => 'yelp'
                    ));

                }

            }
        }
    }

    function api_url($yelp_business_id, $reviews_lang = '') {
        $url = BRB_YELP_API . '/' . $yelp_business_id . '/reviews';
        if (strlen($reviews_lang) > 0) {
            $url = $url . '?locale=' . $reviews_lang;
        }
        return $url;
    }

}