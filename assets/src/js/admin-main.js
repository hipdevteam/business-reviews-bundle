jQuery(document).ready(function($) {

    $('.brb-admin-page a.nav-tab').on('click', function(e)  {
        let $this = $(this), activeId = $this.attr('href');
        $(activeId).show().siblings('.tab-content').hide();
        $this.addClass('nav-tab-active').siblings().removeClass('nav-tab-active');
        e.preventDefault();
    });

    var chart = null;

    const ctx = document.getElementById('rpi-chart');

    if (ctx) {
        let $period   = $('#period'),
            $interval = $('#interval'),
            $place    = $('#place');

        $period.change(function() {
            getStats();
        });
        $interval.change(function() {
            getStats();
        });
        $place.change(function() {
            getStats();
        });

        function getStats() {
            $.post({
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                data: {
                    conn_id: window.place.value,
                    period: window.period.value,
                    interval: window.interval.value,
                    action: 'brb_stats'
                },
                success: function(res) {
                    chart = chartUpdate(chart, ctx, res);

                    //
                    // Render reviews
                    //
                    let $rpi = $('.rpi'),
                        $rpicards = $('.rpi-cards', $rpi),
                        Common = RichPlugins.Common($rpi[0], {});

                    $rpicards.html('');
                    $.each(res.reviews, function(i, rev) {
                        let el = document.createElement('div'),
                            rb = document.createElement('span');

                        el.className = 'rpi-card';
                        rb.innerHTML = 'Reply';
                        rb.className = 'rpi-reply-btn';
                        rb.onclick = function() {
                            let comment = prompt('Please type your reply', rev.reply);
                            if (comment && comment != rev.reply) {
                                $.post({
                                    url      : ajaxurl,
                                    type     : 'POST',
                                    dataType : 'json',
                                    data     : {
                                        action  : 'brb_reply',
                                        conn_id : $place.val(),
                                        rev_id  : rev.id,
                                        comment : comment
                                    },
                                    success  : function(res) {
                                        rev.reply = res.comment;
                                        Common.reviewReply(el, res.comment);
                                        console.log(res);
                                    }
                                });
                            }
                        };

                        Common.convertReviewEl(el, rev);
                        el.appendChild(rb);
                        $rpicards[0].appendChild(el);
                    });
                }
            });
        }
        getStats();
    }

    /*var $statsTxt = $('#stats');
    if ($statsTxt.length) {
        let chart = null,
            $months = $('#grw-overview-months'),
            $places = $('#grw-overview-places'),
            $rating  = $('#grw-overview-rating'),
            $stats = $('#grw-overview-stats'),
            $reviews = $('#grw-overview-reviews');

        $months.change(function() {
            ajaxChartRender();
        });

        $places.change(function() {
            ajaxChartRender();
        });

        function ajaxChartRender() {
            //chart.updateOptions({series: [], noData: { text: 'Loading...' }});
            $.post({
                url      : ajaxurl,
                type     : 'POST',
                dataType : 'json',
                data     : {action: 'brb_stats', conn_id: $places.val()},
                success  : function(res) {
                    chartUpdate(res);
                }
            });
        }

        function chartUpdate(json_data) {
            let json = json_data || JSON.parse($statsTxt.val()),

                json_ratings = json.ratings,
                json_reviews_new = json.reviews_new,
                json_reviews_count = json.reviews_count,

                rating = parseFloat(json.grow.rating).toFixed(1),
                review_count = json.grow.total,

                // Stats parameters
                start_using = parseInt(json.grow.date),
                using_period = new Date().getTime() - start_using,

                rating_after = parseFloat(json.grow.after.rating).toFixed(1),
                review_count_after = json.grow.after.total,

                rating_before = parseFloat(json.grow.before.rating).toFixed(1),
                review_count_before = json.grow.total - review_count_after,

                rating_increse = rating_after - rating_before;
                review_count_increse = parseInt((review_count_after * 100) / review_count_before);

            // Calculate stats for specific period
            let now = new Date(),
                period_end = now,
                data_ratings = [],
                data_reviews_new = [],
                data_reviews_count = [];

            period_end.setDate(1);
            period_end.setHours(0);
            period_end.setMinutes(0);
            period_end.setSeconds(0);
            period_end.setMonth(period_end.getMonth() - $months.val());

            for (let i = 0; now.getTime() > period_end.getTime(); i++) {
                now.setMonth(now.getMonth() - i);
            }

            for (let i = 0; i < json_reviews_count.length; i++) {
                if (period_end < new Date(json_reviews_count[i].x)) {
                    data_ratings.push(json_ratings[i]);
                    data_reviews_new.push(json_reviews_new[i]);
                    data_reviews_count.push(json_reviews_count[i]);
                }
            }

            if (chart) {
                chart.data.datasets[0].data = data_ratings;
                chart.data.datasets[1].data = data_reviews_count;
                chart.data.datasets[2].data = data_reviews_new;
                chart.update();
            } else {
                chartRender(data_ratings, data_reviews_new, data_reviews_count);
            }

            //
            // Render rating
            //
            let $opt = $places.find('option:selected');
            $rating.html(
            '<div class="rpi">' +
                '<div class="rpi-header">' +
                    '<div class="rpi-info">' +
                        '<div class="rpi-name rpi-logo rpi-logo-' + $opt.attr('data-platform') + '">' + $opt.text() + '</div>' +
                        '<div class="rpi-score">' +
                            '<div class="rpi-grade">' + rating + '</div>' +
                            '<div class="rpi-stars">' + RichPlugins.Utils.stars(rating) + '</div>' +
                        '</div>' +
                        '<div class="rpi-based">Based on ' + review_count + ' reviews</div>' +
                        (json.synced ?
                        '<div class="wp-google-powered">Last sync: ' +
                            '<span class="wp-google-time">' +
                                WPacTime.getTime(parseInt(json.synced), _rplg_lang(), 'ago') +
                            '</span>' +
                        '</div>' : '') +
                    '</div>' +
                '</div>' +
            '</div>');

            //
            // Render stats
            //
            $stats.html(
                '<div>' +
                    'Start using: ' +
                    '<span class="grw-stat-val">' + ms2my(start_using) + '</span>' +
                '</div>' +
                '<div>' +
                    'Usage time: ' +
                    '<span class="grw-stat-val">' + ms2dmy(using_period) + '</span>' +
                '</div>' +
                '<div>' +
                    'Rating received' +
                    '<span class="grw-stat-val grw-stat-' + (rating_after < 0 ? 'down' : (rating_after > 0 ? 'up' : '')) + '">' +
                        rating_after +
                    '</span>' +
                '</div>' +
                '<div>' +
                    'New reviews received' +
                    '<span class="grw-stat-val grw-stat-' + (review_count_after < 0 ? 'down' : (review_count_after > 0 ? 'up' : '')) + '">' +
                        review_count_after +
                    '</span>' +
                '</div>' +
                '<div>' +
                    'Increases reviews by' +
                    '<span class="grw-stat-val grw-stat-' + (review_count_increse < 0 ? 'down' : (review_count_increse > 0 ? 'up' : '')) + '">' +
                        review_count_increse + '%' +
                    '</span>' +
                '</div>');

        // TODO HERE

        chartUpdate();
    }*/

    function ms2my(s) {
        let d = new Date(s),
            m = d.toLocaleString('default', { month: 'short'});
        return m + ' ' + d.getFullYear();
    }

    function ms2dmy(s) {
        let d = (s / (1000 * 60 * 60 * 24)).toFixed(0);
        if (d > 30) {
            if (d > 365) {
                return Math.round(d / 365) + ' years';
            }
            return Math.round(d / 30) + ' months';
        }
        return d + ' days';
    }

});

function getInterval(p, i) {
    if (i < 1) return i;
    switch (p) {
        case 'm': return i;
        case 'y': return i < 12 ? 1 : i / 12;
        case 'd': return i < 12 ? i * 30 : (i / 12) * 365;
    }
}

function scalesX(period) {
    switch (period) {
        case 'd':
            return {
                type: 'time',
                time: {
                    unit: 'day',
                    unitStepSize: 1,
                    round: 'day',
                    tooltipFormat: 'DD MMMM, Y',
                    displayFormats: {
                        day: 'DD MMM YY'
                    },
                    isoWeekday: true,
                    bounds: 'ticks',
                    //min: moment().subtract(12, 'month').startOf('month'),
                    //max: moment().endOf('month'),
                    //stepSize: 1,
                },
                ticks: {
                    callback: function (v) {
                        let d = new Date(v),
                            s = d.toDateString();
                        return [s.substring(2, 4), s.substring(4, 7), s.substring(11, 15)];
                    }
                },
                grid: {
                    display: false
                },
                stacked: true
            };

        case 'm':
            return {
                type: 'time',
                time: {
                    unit: 'month',
                    unitStepSize: 1,
                    round: 'month',
                    tooltipFormat: 'MMMM, Y',
                    displayFormats: {
                        month: 'MMM YY'
                    },
                    isoWeekday: true,
                    bounds: 'ticks',
                    //min: moment().subtract(12, 'month').startOf('month'),
                    //max: moment().endOf('month'),
                    //stepSize: 1,
                },
                ticks: {
                    callback: function (v) {
                        let d = new Date(v),
                            s = d.toDateString();
                        return [s.substring(4, 7), s.substring(11, 15)];
                    }
                },
                grid: {
                    display: false
                },
                stacked: true
            };

        case 'y':
            return {
                type: 'time',
                time: {
                    unit: 'year',
                    unitStepSize: 1,
                    round: 'year',
                    tooltipFormat: 'Y',
                    displayFormats: {
                        month: 'YY'
                    },
                    isoWeekday: true,
                    bounds: 'ticks'
                },
                ticks: {
                    callback: function (v) {
                        let d = new Date(v),
                            s = d.toDateString();
                        return [s.substring(11, 15)];
                    }
                },
                grid: {
                    display: false
                },
                stacked: true
            };
    }
}

function getDatasets(stats, period, interval) {
    let i = 0,
        labels = [],
        data_rat = [],
        data_new = [],
        data_total = [],

        stats_keys = Object.keys(stats[period]),

        now = new Date(),
        date_end = interval > 0 ? new moment(now).add(-interval, 'months') : new moment(stats_keys[stats_keys.length - 1]),
        date_cur = new moment(now);

    let stats_date = new moment(stats_keys[i]),
        stats_val = stats[period][stats_keys[i]];

    while (date_cur.isAfter(date_end)) {

        if (date_cur.isBefore(stats_date)) {
            stats_date = new moment(stats_keys[++i]);
            stats_val = stats[period][stats_keys[i]];
        }

        data_rat.push(stats_val.rating);
        data_new.push(stats_val.reviews_new);
        data_total.push(stats_val.reviews_count);

        switch (period) {
            case 'd':
                labels.push(date_cur.format('YYYY-MM-DD'));
                date_cur.add(-1, 'd');
                break;
            case 'm':
                labels.push(date_cur.format('YYYY-MM-01'));
                date_cur.add(-1, 'M');
                break;
            case 'y':
                labels.push(date_cur.format('YYYY'));
                date_cur.add(-1, 'y');
                break;
        }
    }
    return {
        labels: labels,
        data_rat: data_rat,
        data_new: data_new,
        data_total: data_total
    }
}

function chartUpdate(chart, ctx, json) {
    let period = window.period.value,
        interval = window.interval.value,
        datasets = getDatasets(json.stats, period, interval);

    if (chart) {
        chart.data.labels = datasets.labels;
        chart.data.datasets[0].data = datasets.data_total;
        chart.data.datasets[1].data = datasets.data_new;
        chart.data.datasets[2].data = datasets.data_rat;

        if ((interval < 1 && period != 'y') || period == 'd') {
            chart.data.datasets[2].borderWidth = 1;
            chart.data.datasets[2].pointRadius = 1;
            chart.data.datasets[2].hoverRadius = 1;
            chart.data.datasets[0].datalabels.font.size = 0;
            chart.data.datasets[1].datalabels.font.size = 0;
            chart.data.datasets[2].datalabels.font.size = 0;
        } else {
            chart.data.datasets[2].borderWidth = 2;
            chart.data.datasets[2].pointRadius = 12;
            chart.data.datasets[2].hoverRadius = 14;
            chart.data.datasets[0].datalabels.font.size = 10;
            chart.data.datasets[1].datalabels.font.size = 10;
            chart.data.datasets[2].datalabels.font.size = 10;
        }

        chart.options.scales.xCommon = scalesX(period);
        chart.update();
        return chart;
    } else {
        Chart.register(ChartDataLabels);
        return new Chart(ctx, {
            type: 'bar',
            backgroundColor: '#ffffff',
            data: {
                labels: datasets.labels,
                datasets: [{
                    type: 'bar',
                    data: datasets.data_total,
                    label: 'Total reviews',
                    xAxisID: 'xCommon',
                    yAxisID: 'yReviews',
                    backgroundColor: '#6ca6eb',
                    borderWidth: 0,
                    datalabels: {
                        color: '#ffffff',
                        font: {
                            size: 10,
                        }
                    }
                }, {
                    type: 'bar',
                    data: datasets.data_new,
                    label: 'New reviews',
                    xAxisID: 'xCommon',
                    yAxisID: 'yReviews',
                    backgroundColor: '#75ce55',
                    borderWidth: 0,
                    datalabels: {
                        color: '#000000',
                        font: {
                            size: 10,
                        }
                    }
                }, {
                    type: 'line',
                    data: datasets.data_rat,
                    label: 'Rating',
                    xAxisID: 'xCommon',
                    yAxisID: 'yRating',
                    fill: false,
                    lineTension: 0.4,
                    borderColor: '#fb7900',
                    borderWidth: 2,
                    backgroundColor: '#fb7900',
                    pointRadius: 12,
                    hoverRadius: 14,
                    pointBackgroundColor: '#fb7900',
                    pointBorderWidth: 0,
                    datalabels: {
                        color: '#ffffff',
                        anchor: 'center',
                        align: 'center',
                        textAlign: 'center',
                        font: {
                            size: 10,
                        },
                        formatter: function (value, context) {
                            return value > 0 ? value : '';
                        }
                    },
                }]
            },
            options: {
                radius: 4,
                barPercentage: .75,
                interaction: {
                    mode: 'index',
                    intersect: !1
                },
                responsive: !0,
                plugins: {
                    legend: {
                        display: true,
                        position: 'bottom',
                        align: 'start',
                        title: {
                            display: true,
                            padding: 4
                        }
                    },
                    tooltip: !1,
                    ChartDataLabels,
                    //verticalLinePlugin
                },
                scales: {
                    xCommon: scalesX(period),
                    yRating: {
                        type: 'linear',
                        beginAtZero: true,
                        suggestedMax: 5,
                        ticks: {
                            display: true,
                            maxTicksLimit: 11,
                            stepSize: 1
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Rating',
                            fontColor: '#f1b53d',
                            fontSize: '8'
                        },
                        grid: {
                            display: true,
                            color: '#ffffff'
                        },
                        border: {
                            display: false,
                        },
                        position: 'right',
                    },
                    yReviews: {
                        display: true,
                        datalabels: {
                            color: '#007bff',
                            anchor: 'end',
                            align: 'end',
                            textAlign: 'center',
                            formatter: function (value, context) {
                                return value > 0 ? value : '';
                            }
                        },
                        beginAtZero: true,
                        ticks: {
                            display: true,
                            maxTicksLimit: 11,
                            stepSize: 5,
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Reviews',
                            fontColor: 'green',
                            fontSize: '8',
                        },
                        grid: {
                            display: false,
                            color: '#ffffff'
                        },
                        border: {
                            display: false,
                        },
                        grace: '25%',
                        stacked: true
                    }
                }
            }
        });
    }
}