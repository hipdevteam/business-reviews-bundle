var RichPlugins = RichPlugins || {

    /**
     * Tag layout
     */
    Tag: function(rootEl, options) {

        const id = rootEl.getAttribute('data-id');
        const cntEl = rootEl.getElementsByClassName('rpi-tag-cnt')[0];
        const tagOpts = JSON.parse(cntEl.getAttribute('data-opts'));

        var This = null;

        return This = {

            init: function() {
                let starsReviewUsEl = rootEl.querySelector('.rpi-stars[data-reviewus]');
                if (starsReviewUsEl) {
                    This.starsInit(starsReviewUsEl); // TODO
                    starsReviewUsEl.onclick = function(e) {
                        var svg = e.target.tagName == 'svg' ? e.target : e.target.parentNode,
                            idx = [...svg.parentNode.children].indexOf(svg);
                        rpi.Utils.popup(idx > 2 ? this.getAttribute('data-reviewus') : 'https://app.richplugins.com/feedback?s=' + idx, 800, 600);
                    };
                }

                if (tagOpts.tag_popup > 0) {
                    setTimeout(function() {
                        rootEl.className += ' rpi-pop-up';
                    }, tagOpts.tag_popup * 1000);
                }

                if (tagOpts.tag_click == 'sidebar') {

                    let sbId = 'rpi-tag-sidebar-' + id,
                        sbRootEl = document.getElementById(sbId);

                    if (!sbRootEl) {
                        sbRootEl = document.createElement('div');
                        sbRootEl.id = sbId;
                        sbRootEl.className = 'rpi';
                        sbRootEl.innerHTML = '' +
                            '<div class="rpi-sb" data-layout="' + tagOpts.tag_sidebar + '" style="display:none;width:0">' +
                                '<div class="rpi-sbb"></div>' +
                                '<div class="rpi-sbc rpi-scroll">' +
                                    '<div class="rpi-sbci"></div>' +
                                '</div>' +
                                '<div class="rpi-sbx"></div>' +
                            '</div>';
                        document.body.appendChild(sbRootEl);
                    }

                    let sbEl = sbRootEl.getElementsByClassName('rpi-sb')[0],
                        sbxEl = sbEl.getElementsByClassName('rpi-sbx')[0];

                    sbxEl.onclick = function(e) {
                        rpi.View.widthAnim(sbEl, 368, 300);
                    };

                    rootEl.onclick = function(e) {
                        rpi.View.widthAnim(sbEl, 368, 300);

                        let sbciEl = sbEl.getElementsByClassName('rpi-sbci')[0];
                        if (sbciEl.innerHTML == '') {
                            let url  = brb_vars.ajaxurl + '?action=brb_embed&brb_collection_id=' + id + '&brb_view_mode=' + tagOpts.tag_sidebar;
                            rpi.Utils.ajax(url, 'POST', function(json) {
                                sbciEl.innerHTML = json.data;
                                RichPlugins.init(sbciEl.querySelector('.rpi[data-exec=""]'));
                            });
                        }
                    };
                }
                console.log('RichPlugins Tag initialized');
            },

            starsInit: function(starsEl) {
                let starsInfo = starsEl.getAttribute('data-info').split(',');
                starsEl.innerHTML = render_stars(starsInfo[0], starsInfo[1], starsInfo[2]);
            }
        }
    },

    /**
     * Flash layout
     */
    Flash: function(rootEl, options) {
        return This = {
            init: function() {
                rpi.Flash(rootEl, options);
            }
        }
    },

    /**
     * List & Grid layouts
     */
    List: function(rootEl, rootOpts) {
        const common = rpi.Common(rootEl, rootOpts, {
            time     : 'rpi-time',
            text     : 'rpi-text',
            readmore : 'rpi-readmore'
        });
        const column = rpi.Column(rootEl, rootOpts, {
            cnt      : 'rpi-cnt',
            col      : 'rpi-col',
            card     : 'rpi-card'
        });
        const view = rpi.View(rootEl, rootOpts, {
            common: common
        });

        const reviewsEl = rootEl.querySelector('.rpi-content .rpi-cards');

        var This = null;

        return This = {

            init: function() {
                common.init();
                column.init();
                view.init();
                This.actions();
                console.log('RichPlugins List initialized');
            },

            actions: function() {
                var more = rootEl.getElementsByClassName('rpi-url')[0];
                if (more) {
                    more.onclick = function() {
                        view.loadNextReviews(rootOpts.pagination, function() {
                            let count = parseInt(reviewsEl.getAttribute('data-count')),
                                offset = parseInt(reviewsEl.getAttribute('data-offset'));
                            if (offset >= count) {
                                rpi.Utils.rm(more);
                            }
                        });
                        return false;
                    }
                }
            }
        }
    },

    /**
     * Slider layout
     */
    Slider: function(rootEl, rootOpts) {
        const common = rpi.Common(rootEl, rootOpts, {
            time     : 'rpi-time',
            text     : 'rpi-text',
            readmore : 'rpi-readmore'
        });
        const column = rpi.Column(rootEl, rootOpts, {
            cnt      : 'rpi-cnt',
            col      : 'rpi-col',
            card     : 'rpi-card'
        });
        const view = rpi.View(rootEl, rootOpts, {
            common: common
        });

        const cnt  = rootEl.getElementsByClassName('rpi-cnt')[0];
        const opts = JSON.parse(cnt.getAttribute('data-opts'));

        const slider = rpi.Slider(rootEl, opts, {
            cnt      : 'rpi-cnt',
            col      : 'rpi-col',
            content  : 'rpi-content',
            cards    : 'rpi-cards',
            card     : 'rpi-card',
            text     : 'rpi-text',
            btnPrev  : 'rpi-slider-prev',
            btnNext  : 'rpi-slider-next',
            dotsWrap : 'rpi-dots-wrap',
            dots     : 'rpi-dots',
            dot      : 'rpi-dot'
        }, {
            view: view,
            column: column
        });

        var This = null;

        return This = {
            init: function() {
                slider.init(function() {
                    common.init();
                }, function() {
                    view.init();
                    console.log('RichPlugins slider initialized');
                });
            }
        };
    },

    init: function(rootEl) {
        rootEl.setAttribute('data-exec', '1');
        let opts = JSON.parse(rootEl.getAttribute('data-opts')),
            layout = rpi.Utils.capit(opts.layout);
        if (layout == 'List' || layout == 'Grid') {
            RichPlugins.List(rootEl, opts).init();
        } else {
            RichPlugins[layout](rootEl, opts).init();
        }
    }
};

document.addEventListener('DOMContentLoaded', function() {
    const els = document.querySelectorAll('.rpi[data-exec=""]');
    if (els.length) {
        rpi.Utils.clear();
        for (var i = 0; i < els.length; i++) {
            RichPlugins.init(els[i]);
        }
    }
});